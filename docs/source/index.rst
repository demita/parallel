lzmp -- multiprocessing for the lazy
====================================

``lzmp`` is a simple wrapper around the ``multiprocessing`` module,
allowing the lazy programmer to run batches of processes.

``lzmp`` contains the class :class:`.Pool` which lets the user specify
one or more callable objects (such as functions) along with lists of
arguments to process. ``lzmp`` collects the return value of each call
and returns the whole lot as a list, keeping the original submission
order. For a single type of callable, the standalone function
:meth:`~lzmp.run` wraps the wrapper and allows one-line parallelization.

Installation
------------

> pip install lzmp

Reference
---------

.. autofunction:: lzmp.run

.. autofunction:: lzmp.wrap

.. autoclass:: lzmp.Pool
    :members:

History
-------

- 0.4.0: Add *post* argument.

- 0.3.2: Fix another bug in the section checking errors.

- 0.3.1: Fix a bug in the section checking errors (bug present in 0.3.0).

- 0.3.0: Close forked processes while running to avoid OS saturation and
         add the *extra* argument to :meth:`~lzmp.wrap`.

- 0.2.0: Add the *echo* argument.

- 0.1.0: Initial release.
